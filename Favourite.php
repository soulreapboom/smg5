<?php
require_once 'includes/core/db.php';
session_start();
if (isset($_SESSION['user'])) {
    $user = $_SESSION['user'];
    $query = "SELECT product_id FROM favourite WHERE user_id = '{$user['id']}'";
    $response = mysqli_query($db, $query);
    $favourite = mysqli_fetch_all($response);
    if (count($favourite) > 0) {
        $favourite = call_user_func_array('array_merge', $favourite);
        $favourite = implode(',', $favourite);
        $query = "SELECT * FROM products WHERE id IN ($favourite)";
        $response = mysqli_query($db, $query);
        $products = mysqli_fetch_all($response, 1);
    }
} else {
    header("Location: login.php");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="Favourite.css">
    <title>Избранное</title>
</head>
<body>
    <header>
        <div class="HLeft">
            <div class="logotype"><a href="index.php"><img src="images/Logotype.png" class="logo"></a></div>
            <div class="number">Горячая линия:<br>8 (xxx) xxx-xx-xx</div>
        </div>
        <div class="HCenter">
            <div class="group_button">
                <form action="login.php">
                    <button class="login">Вход</button>
                </form>
                <form action="register.php">
                    <button class="registration">Регистрация</button>
                </form>
            </div>
        </div>
        <div class="HRight">
            <div class="flex1">
                <div class="flex11">
                    <div class="menu">
                        <ul>
                            <li><a href="index.php">Главная</a></li>
                            <li><a href="MainTovar.php">Товары</a></li>
                            <li><a href="services.php">Услуги</a></li>
                            <li><a href="aboutUs.php">О нас</a></li>
                            <li><a href="contacts.php">Контакты</a></li>
                            <li><a href="news.php">Новости</a></li>
                        </ul>
                    </div>
                </div>
                <div class="flex12">
                    <form action="">
                        <input for=""></input>
                        <button><div class="search_icon"></div></button>
                    </form>
                    <div class="favourite"><div class="favourite_icon"><a href="#"></a></div></div>
                    <div class="basket"><div class="basket_icon"></div></div>
                </div>
            </div>
        </div>
    </header>
    <main>
      <div class="padding"></div>
      <div class="login_header"><p>Избранное</p></div>
      <div class="flex_tovars">

          <?php
          if(isset($products)) {
          foreach ($products as $product) { ?>
              <div class="popular_block">
                  <div class="popular_block2">
                      <div class="popular_image0" style="background-image: url('admin/uploads/<?= $product['image'] ?>')">
                          <div class="popular_image">
                              <a href="product.php?id=<?= $product['id'] ?>" class="more">Подробнее</a>
                          </div>
                      </div>
                      <div class="name"><?= $product['title'] ?></div>
                      <div class="price"><?= $product['price'] ?></div>
                      <div class="popular_item_flex">
                          <a href="includes/add_favourite.php?id=<?= $product['id'] ?>" class="popular_favourite"></a>
                          <a href="includes/add_basket.php?id=<?= $product['id'] ?>" class="popular_basket"></a>
                      </div>
                  </div>
              </div>
          <?php } } else { ?>
          <div style="text-align: center">У вас нет товаров в избранном</div>
          <?php } ?>
         </div>
      </div>
      </main>
      <footer>
        <div class="footer_top">
            <div class="flex_footer">
                <div class="footer_block">
                    <div class="footer_block_content1"><img src="images/VKontakte Icon.png" alt=""> <span>https://vk.com/public211234678</span></div>
                    <div class="footer_block_content1"><img src="images/Instagram Icon.png" alt=""> <span>https://instagram.com/xxxxxx</span></div>
                    <div class="footer_block_content1"><img src="images/Mail Icon.png" alt=""> <span>VashaOptika@mail.ru</span></div>
                </div>
                <div class="footer_block">
                    <div class="footer_text">Горячая линия:<br>8(xxx) xxx-xx-xx</div>
                </div>
            </div>
        </div>
        <div class="footer_bottom">© 2012-2022, все права защищены.</div>
    </footer>
      </body>
      </html>