<?php
require_once 'includes/core/db.php';
session_start();

$query = "SELECT * FROM products ORDER BY RAND() LIMIT 3";
$response = mysqli_query($db, $query);
$products = mysqli_fetch_all($response, 1);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="styles.css">
    <title>Главная</title>
</head>
<body>
    <header>
        <div class="HLeft">
            <div class="logotype"><a href="#"><img src="images/Logotype.png" class="logo"></a></div>
            <div class="number">Горячая линия:<br>8 (xxx) xxx-xx-xx</div>
        </div>
        <div class="HCenter">
            <div class="group_button">
                <?php if(isset($_SESSION['user'])) { ?>
                <span style="text-align: center; color: #fff; display: block;"><?= $_SESSION['user']['login'] ?></span>
                <form action="includes/logout.php">
                    <button class="login">Выйти</button>
                </form>
                <?php if($_SESSION['user']['is_admin']) { ?>
                <form action="admin/index.php">
                    <button class="login">Админ-панель</button>
                </form>
                <?php } } else { ?>
                <form action="login.php">
                    <button class="login">Вход</button>
                </form>
                <form action="register.php">
                    <button class="registration">Регистрация</button>
                </form>
                <?php } ?>
            </div>
        </div>
        <div class="HRight">
            <div class="flex1">
                <div class="flex11">
                    <div class="menu">
                        <ul>
                            <li><a href="#" style="color: #F3E243; text-shadow: 0 0 10px #F3E243;">Главная</a></li>
                            <li><a href="MainTovar.php">Товары</a></li>
                            <li><a href="services.php">Услуги</a></li>
                            <li><a href="aboutUs.php">О нас</a></li>
                            <li><a href="contacts.php">Контакты</a></li>
                            <li><a href="news.php">Новости</a></li>
                        </ul>
                    </div>
                </div>
                <div class="flex12">
                    <form action="">
                        <input for=""></input>
                        <button><div class="search_icon"></div></button>
                    </form>
                    <div class="favourite"><a href="Favourite.php"><div class="favourite_icon"></div></a></div>
                    <div class="basket"><a href="Basket.php"><div class="basket_icon"></div></a></div>
                </div>
            </div>
        </div>
    </header>
    <main>
            <div class="beginning">
                <div class="beginning_slogan">Спасибо, что выбираете<br>"<span style="color: #F3E243;">Ваша Оптика</span>"</div>
            </div>

            <div class="advantages">
                <div class="advantages_slogan">Наши преимущества</div>
                <div class="advantages_flex">
                    <div class="advantage_block">
                        <div class="advantage_icon"><img src="images/Vector.png" alt="" style="width: 150px; height: 123px;"></div>
                        <div class="advantage_text1">Рейтинг</div>
                        <div class="advantage_text2">Нам нравится развиваться,<br>и мы готовы улучшать наше качество<br>обслуживания</div>
                    </div>
                    <div class="advantage_block">
                        <div class="advantage_icon"><img src="images/Ruble.png" alt="" style="width: 123px; height: 123px;"></div>
                        <div class="advantage_text1">Хорошие цены</div>
                        <div class="advantage_text2">Приобрести наши товары<br>может каждый! Также регулярно<br>обновляются скидки на товары!</div>
                    </div>
                    <div class="advantage_block">
                        <div class="advantage_icon"><img src="images/Book.png" alt="" style="width: 188px; height: 123px;"></div>
                        <div class="advantage_text1">Врач-офтальмолог</div>
                        <div class="advantage_text2">В нашей компании есть<br>квалифицированный<br>врач-офтальмолог</div>
                    </div>
                </div>
            </div>

            <div class="popular_items">
                <div class="popular_slogan">Популярные товары</div>
                <div class="popular_flex">

                    <?php foreach ($products as $product) { ?>
                    <div class="popular_block">
                        <div class="popular_block2">
                            <div class="popular_image0" style="background-image: url('admin/uploads/<?= $product['image'] ?>')">
                                <div class="popular_image">
                                    <a href="product.php?id=<?= $product['id'] ?>" class="more">Подробнее</a>
                                </div>
                            </div>
                            <div class="name"><?= $product['title'] ?></div>
                            <div class="price"><?= $product['price'] ?></div>
                            <div class="popular_item_flex">
                                <a href="includes/add_favourite.php?id=<?= $product['id'] ?>" class="popular_favourite"></a>
                                <a href="includes/add_basket.php?id=<?= $product['id'] ?>" class="popular_basket"></a>
                            </div>
                         </div>
                     </div>
                    <?php } ?>

                </div>
            </div>

            <div class="advertisiments">
                <div class="ADS_slogan">Наши акции</div>
                <div id="ADSslider">
                    <ul id="ADSslideWrap"> 
                    <li><img src="images/slider_frame.png" alt=""></li>
                    <li><img src="images/slider_frame.png" alt=""></li>
                    <li><img src="images/slider_frame.png" alt=""></li>
                    <li><img src="images/slider_frame.png" alt=""></li>
                    <li><img src="images/slider_frame.png" alt=""></li>
                    </ul>
                    <button id="ADSprev1">&#8810;</button>
                    <button id="ADSnext1">&#8811;</button>
                </div>
              </div>

    </main>

    <footer>
        <div class="footer_top">
            <div class="flex_footer">
                <div class="footer_block">
                    <div class="footer_block_content1"><img src="images/VKontakte Icon.png" alt=""> <span>https://vk.com/public211234678</span></div>
                    <div class="footer_block_content1"><img src="images/Instagram Icon.png" alt=""> <span>https://instagram.com/xxxxxx</span></div>
                    <div class="footer_block_content1"><img src="images/Mail Icon.png" alt=""> <span>VashaOptika@mail.ru</span></div>
                </div>
                <div class="footer_block">
                    <div class="footer_text">Горячая линия:<br>8(xxx) xxx-xx-xx</div>
                </div>
            </div>
        </div>
        <div class="footer_bottom">© 2012-2022, все права защищены.</div>
    </footer>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"></script>
    <script src="script.js"></script>
</body>
</html>