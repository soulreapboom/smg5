<?php
session_start();

$message = isset($_SESSION['message']) ? $_SESSION['message'] : '';
unset($_SESSION['message']);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
      <link rel="stylesheet" href="register.css">
      <title>Регистрация</title>
  </head>
    <body>
        <header>
          <div class="HLeft">
            <div class="logotype"><a href="index.php"><img src="images/Logotype.png" class="logo"></a></div>
              <div class="number">Горячая линия:<br>8 (xxx) xxx-xx-xx</div>
          </div>
          <div class="HCenter">
              <div class="group_button">
                  <form action="login.php">
                      <button class="login">Вход</button>
                  </form>
                  <form action="register.php">
                      <button class="registration" style="color: black; background-color:white;">Регистрация</button>
                  </form>
              </div>
          </div>
          <div class="HRight">
              <div class="flex1">
                  <div class="flex11">
                      <div class="menu">
                          <ul>
                            <li><a href="index.php">Главная</a></li>
                            <li><a href="MainTovar.php">Товары</a></li>
                            <li><a href="services.php">Услуги</a></li>
                            <li><a href="aboutUs.php">О нас</a></li>
                            <li><a href="contacts.php">Контакты</a></li>
                            <li><a href="news.php">Новости</a></li>
                          </ul>
                      </div>
                  </div>
                  <div class="flex12">
                      <form action="">
                          <input for=""></input>
                          <button><div class="search_icon"></div></button>
                      </form>
                      <div class="favourite"><div class="favourite_icon"></div></div>
                      <div class="basket"><div class="basket_icon"></div></div>
                  </div>
              </div>
          </div>
        </header>
        <main>
          <div class="padding"></div>
        <div class="login_header"><p>Регистрация</p></div>
        <div class="login_form">
          <form action="includes/register.php" method="post">
              <p>Чтобы авторизироваться,<br>введите данные своей учётной записи</p>
              <label for="">Почта Mail<br></label>
              <input type="text" placeholder="Например Ivanow@mail.ru" name="email">
              <label for="">Логин<br></label>
              <input type="text" placeholder="Введите логин" name="login">
              <label for="">Пароль<br></label>
              <input type="password" class="pass" name="password" placeholder="Введите пароль"><button class="eye"><img src="images/passeye.png" alt=""></button>
              <label for="">Повторите пароль<br></label>
              <input type="password" class="pass" name="password_confirmation" placeholder="Подтвердите пароль"><button class="eye"><img src="images/passeye.png" alt=""></button>
              <span style="text-align: center; display: block;"><?= $message ?></span>
              <button class="logGo">Зарегистрироваться</button>
          </form>
          <div class="log_question">
              <div class="maybe_reg"><p>Уже есть аккаунт?</p><a href="login.php">Войти</a></div>
          </div>
      </div>
        </main>
        <footer>
          <div class="footer_top">
              <div class="flex_footer">
                  <div class="footer_block">
                      <div class="footer_block_content1"><img src="images/VKontakte Icon.png" alt=""> <span>https://vk.com/public211234678</span></div>
                      <div class="footer_block_content1"><img src="images/Instagram Icon.png" alt=""> <span>https://instagram.com/xxxxxx</span></div>
                      <div class="footer_block_content1"><img src="images/Mail Icon.png" alt=""> <span>VashaOptika@mail.ru</span></div>
                  </div>
                  <div class="footer_block">
                      <div class="footer_text">Горячая линия:<br>8(xxx) xxx-xx-xx</div>
                  </div>
              </div>
          </div>
          <div class="footer_bottom">© 2012-2022, все права защищены.</div>
      </footer>
    </body>
    </html>