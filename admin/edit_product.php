<?php
require_once 'includes/core/db.php';

$query = "SELECT * FROM categories";
$response = mysqli_query($db, $query);
$categories = mysqli_fetch_all($response, 1);

$id = $_GET['id'];

$query = "SELECT * FROM products WHERE id = '$id'";
$response = mysqli_query($db, $query);
$product = mysqli_fetch_assoc($response);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Админ-панель</title>
</head>
<body>
<div>
    <a href="../index.php">Главная</a>
    <a href="index.php">Админ-панель</a>
    <a href="products.php">Товары</a>
    <a href="services.php">Услуги</a>
    <a href="aboutus.php">О нас</a>
    <a href="contacts.php">Контакты</a>
    <a href="news.php">Новости</a>
</div>
<div>
    <form action="includes/update_product.php" method="post" enctype="multipart/form-data">
        <input type="text" placeholder="заголовок" name="title" value="<?= $product['title'] ?>">
        <textarea name="text" placeholder="описание"><?= $product['text'] ?></textarea>
        <input type="text" placeholder="стоимость" name="price" value="<?= $product['price'] ?>">
        <select name="category">
            <?php foreach ($categories as $category){ ?>
            <option value="<?= $category['id'] ?>" <?= $category['id'] === $product['category'] ? 'selected' : '' ?>><?= $category['name'] ?></option>
            <?php } ?>
        </select>
        <input type="file" name="image">
        <input type="hidden" name="id" value="<?= $product['id'] ?>">
        <button>Сохранить</button>
    </form>
</div>
</body>
</html>