<?php
require_once 'includes/core/db.php';

$id = $_GET['id'];

$query = "SELECT * FROM news WHERE id = '$id'";
$response = mysqli_query($db, $query);
$new = mysqli_fetch_assoc($response);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Админ-панель</title>
</head>
<body>
<div>
    <a href="../index.php">Главная</a>
    <a href="index.php">Админ-панель</a>
    <a href="products.php">Товары</a>
    <a href="services.php">Услуги</a>
    <a href="aboutus.php">О нас</a>
    <a href="contacts.php">Контакты</a>
    <a href="news.php">Новости</a>
</div>
<div>
    <form action="includes/update_new.php" method="post" enctype="multipart/form-data">
        <input type="text" placeholder="заголовок" name="title" value="<?= $new['title'] ?>">
        <textarea name="text" placeholder="текст"><?= $new['text'] ?></textarea>
        <input type="file" name="image">
        <input type="hidden" name="id" value="<?= $new['id'] ?>">
        <button>Сохранить</button>
    </form>
</div>
</body>
</html>