<?php

require_once 'includes/core/db.php';

$query = "SELECT * FROM categories";
$response = mysqli_query($db, $query);
$categories = mysqli_fetch_all($response, 1);

$query = "SELECT * FROM products";
$response = mysqli_query($db, $query);
$products = mysqli_fetch_all($response, 1);
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Админ-панель</title>
</head>
<body>
<div>
    <a href="../index.php">Главная</a>
    <a href="index.php">Админ-панель</a>
    <a href="products.php">Товары</a>
    <a href="services.php">Услуги</a>
    <a href="aboutus.php">О нас</a>
    <a href="contacts.php">Контакты</a>
    <a href="news.php">Новости</a>
</div>

<div>
    <a href="create_product.php">Создать товар</a>
</div>

<div>
    <table>
        <tr>
            <th>
                id
            </th>
            <th>
                Заголовок
            </th>
            <th>
                Описание
            </th>
            <th>
                Цена
            </th>
            <th>
                Изображение
            </th>
            <th>
                Категория
            </th>
            <th>
                Действия
            </th>
        </tr>
        <?php foreach ($products as $product) { ?>
        <tr>
            <td><?= $product['id'] ?></td>
            <td><?= $product['title'] ?></td>
            <td><?= $product['text'] ?></td>
            <td><?= $product['price'] ?></td>
            <td><img src="uploads/<?= $product['image'] ?>" style="max-height: 30px" alt=""></td>
            <td><?= $categories[array_search($product['category'], array_column($categories, 'id'))]['name'] ?></td>
            <td>
                <a href="edit_product.php?id=<?= $product['id'] ?>">Редактировать</a>
                <a href="includes/destroy_product.php?id=<?= $product['id'] ?>">Удалить</a>
            </td>
        </tr>
        <?php } ?>
    </table>
</div>

</body>
</html>