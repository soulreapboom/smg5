<?php

require_once 'includes/core/db.php';

$query = "SELECT * FROM contacts";
$response = mysqli_query($db, $query);
$contacts = mysqli_fetch_all($response, 1);
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Админ-панель</title>
</head>
<body>
<div>
    <a href="../index.php">Главная</a>
    <a href="index.php">Админ-панель</a>
    <a href="products.php">Товары</a>
    <a href="services.php">Услуги</a>
    <a href="aboutus.php">О нас</a>
    <a href="contacts.php">Контакты</a>
    <a href="news.php">Новости</a>
</div>

<div>
    <a href="create_contact.php">Создать контакт</a>
</div>

<div>
    <table>
        <tr>
            <th>
                id
            </th>
            <th>
                Текст
            </th>
            <th>
                Ссылка
            </th>
            <th>
                Изображение
            </th>
            <th>
                Действия
            </th>
        </tr>
        <?php foreach ($contacts as $contact) { ?>
            <tr>
                <td><?= $contact['id'] ?></td>
                <td><?= $contact['text'] ?></td>
                <td><?= $contact['link'] ?></td>
                <td><img src="uploads/<?= $contact['image'] ?>" style="max-height: 30px" alt=""></td>
                <td>
                    <a href="edit_contact.php?id=<?= $contact['id'] ?>">Редактировать</a>
                    <a href="includes/destroy_contact.php?id=<?= $contact['id'] ?>">Удалить</a>
                </td>
            </tr>
        <?php } ?>
    </table>
</div>

</body>
</html>