<?php
require_once 'includes/core/db.php';

$query = "SELECT * FROM about";
$response = mysqli_query($db, $query);
$categories = mysqli_fetch_all($response, 1);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Админ-панель</title>
</head>
<body>
<div>
    <a href="../index.php">Главная</a>
    <a href="index.php">Админ-панель</a>
    <a href="products.php">Товары</a>
    <a href="services.php">Услуги</a>
    <a href="aboutus.php">О нас</a>
    <a href="contacts.php">Контакты</a>
    <a href="news.php">Новости</a>
</div>
<div>
    <form action="includes/store_about.php" method="post" enctype="multipart/form-data">
        <textarea name="text" placeholder="описание"></textarea>
        <input type="file" name="image">
        <button>Создать</button>
    </form>
</div>
</body>
</html>