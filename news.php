<?php

require_once 'includes/core/db.php';

$query = "SELECT * FROM news";
$response = mysqli_query($db, $query);
$news = mysqli_fetch_all($response, 1);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="news.css">
    <title>Контакты</title>
</head>
<body>
    <header>
        <div class="HLeft">
            <div class="logotype"><div class="logo"></div></div>
            <div class="number">Горячая линия:<br>8 (xxx) xxx-xx-xx</div>
        </div>
        <div class="HCenter">
            <div class="group_button">
                <form action="login.php">
                    <button class="login">Вход</button>
                </form>
                <form action="register.php">
                    <button class="registration">Регистрация</button>
                </form>
            </div>
        </div>
        <div class="HRight">
            <div class="flex1">
                <div class="flex11">
                    <div class="menu">
                        <ul>
                            <li><a href="index.php">Главная</a></li>
                            <li><a href="MainTovar.php">Товары</a></li>
                            <li><a href="services.php">Услуги</a></li>
                            <li><a href="aboutUs.php">О нас</a></li>
                            <li><a href="contacts.php">Контакты</a></li>
                            <li><a href="news.php" style="color: #F3E243; text-shadow: 0 0 10px #F3E243;">Новости</a></li>
                        </ul>
                    </div>
                </div>
                <div class="flex12">
                    <form action="">
                        <input for=""></input>
                        <button><div class="search_icon"></div></button>
                    </form>
                    <div class="favourite"><a href="Favourite.php"><div class="favourite_icon"></div></a></div>
                    <div class="basket"><div class="basket_icon"></div></div>
                </div>
            </div>
        </div>
    </header>
    <main>
      <div class="padding"></div>
      <div class="login_header"><p>Новости</p></div>

    <div class="news_items">
            <?php foreach ($news as $new) { ?>
            <div class="news_item" style="background-image: url('admin/uploads/<?= $new['image'] ?>'); background-repeat: no-repeat; background-size: 100%">
                <div class="news_dark">
                    <p class="news_slogan"><?= $new['title'] ?></span></p>
                    <form action=""><button class="news_more">Подробнее</button></form>
                </div>
            </div>
            <?php } ?>

    </div>

    </main>
    <footer>
      <div class="footer_top">
          <div class="flex_footer">
              <div class="footer_block">
                  <div class="footer_block_content1"><img src="images/VKontakte Icon.png" alt=""> <span>https://vk.com/public211234678</span></div>
                  <div class="footer_block_content1"><img src="images/Instagram Icon.png" alt=""> <span>https://instagram.com/xxxxxx</span></div>
                  <div class="footer_block_content1"><img src="images/Mail Icon.png" alt=""> <span>VashaOptika@mail.ru</span></div>
              </div>
              <div class="footer_block">
                  <div class="footer_text">Горячая линия:<br>8(xxx) xxx-xx-xx</div>
              </div>
          </div>
      </div>
      <div class="footer_bottom">© 2012-2022, все права защищены.</div>
  </footer>
    </body>
</html>